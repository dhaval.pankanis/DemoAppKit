# DemoAppKit

[![CI Status](http://img.shields.io/travis/dhaval-pankanis/DemoAppKit.svg?style=flat)](https://travis-ci.org/dhaval-pankanis/DemoAppKit)
[![Version](https://img.shields.io/cocoapods/v/DemoAppKit.svg?style=flat)](http://cocoapods.org/pods/DemoAppKit)
[![License](https://img.shields.io/cocoapods/l/DemoAppKit.svg?style=flat)](http://cocoapods.org/pods/DemoAppKit)
[![Platform](https://img.shields.io/cocoapods/p/DemoAppKit.svg?style=flat)](http://cocoapods.org/pods/DemoAppKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DemoAppKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "DemoAppKit"
```

## Author

dhaval-pankanis, dhaval.patel@pankanis.com

## License

DemoAppKit is available under the MIT license. See the LICENSE file for more info.
